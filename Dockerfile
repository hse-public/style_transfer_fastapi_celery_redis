FROM python:3.9.6-slim-buster
ENV PYTHONUNBUFFERED=1
WORKDIR /app
COPY requirements.txt requirements.txt
RUN DEBIAN_FRONTEND="noninteractive" apt-get update --yes
RUN DEBIAN_FRONTEND="noninteractive" apt-get install --yes python3-opencv
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
COPY . /app
EXPOSE 8000