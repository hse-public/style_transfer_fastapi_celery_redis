import os
import subprocess
import time

from celery import Celery
from dotenv import load_dotenv

load_dotenv(".env")

celery_app = Celery(__name__)
celery_app.conf.broker_url = os.environ.get("CELERY_BROKER_URL")
celery_app.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND")


@celery_app.task(name="create_task")
def create_task(a, b, c):
    time.sleep(a)
    return b + c


@celery_app.task(name="style_transfer_tasks")
def style_transfer_task(image_path):
    result = subprocess.run(["python", "./stylize.py", image_path], capture_output=True, text=True)    
    print("stdout:", result.stdout)
    print("stderr:", result.stderr)
