import json
from datetime import timedelta

import aiofiles
from celery.result import AsyncResult
from fastapi import FastAPI, File, UploadFile, status
from fastapi.responses import FileResponse, JSONResponse

from celery_worker import celery_app, style_transfer_task

app = FastAPI()

@app.post("/style_transfer")
async def image_upload(file: UploadFile=File(...)):
    filename = file.filename
    out_file_path = "/app/images/" + f'{filename}'
    try:
        async with aiofiles.open(out_file_path, 'wb') as out_file:
            content = await file.read()  # async read
            await out_file.write(content)  # async write
            # call celery for processing
            task = style_transfer_task.delay(out_file_path)    

    except Exception as e:
        return JSONResponse(
            status_code = status.HTTP_400_BAD_REQUEST,
            content = { 'message' : str(e) }
            )
    else:
        return JSONResponse(
            status_code = status.HTTP_200_OK,
            content = {"task_id": task.id}
            )    

@app.get("/style_transfer/{task_id}")
def check_handler(task_id):
    task = AsyncResult(task_id, app=celery_app)
    if task.ready():
        response = {
            "status": "DONE"
        }
    else:
        response = {
            "status": "IN_PROGRESS"
        }
    return json.dumps(response)

@app.get("/new_style/{image_name}")
def image_download(image_name):
    try:
        image_new_style = f"{image_name}_new_style.jpg" 
        file_path = "/app/images/results/" + image_new_style 
        return FileResponse(path=file_path, media_type='application/octet-stream', filename=image_new_style)
    except Exception as e:
        return JSONResponse(
            status_code = status.HTTP_400_BAD_REQUEST,
            content = { 'message' : str(e) }
            )

