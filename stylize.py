## Adapted 
## From: https://github.com/rrmina/fast-neural-style-pytorch
##

import torch
import utils
import transformer
import os
import sys
from torchvision import transforms
import time
import cv2

if len(sys.argv) < 2: 
    print(f"Usage: stylize.py <content_image_path>")
    sys.exit(1)

content_image_path = sys.argv[1]

if len(sys.argv) == 3:
    style_transform_path = sys.argv[2]
else:
    # default style
    style_transform_path = "transforms/chinese_painting.pth"

STYLE_TRANSFORM_PATH = style_transform_path
PRESERVE_COLOR = False

def stylize(content_image_path):
    # Device
    device = ("cuda" if torch.cuda.is_available() else "cpu")

    # Load Transformer Network
    net = transformer.TransformerNetwork()
    net.load_state_dict(torch.load(STYLE_TRANSFORM_PATH))
    net = net.to(device)

    with torch.no_grad():
        torch.cuda.empty_cache()
        content_image = utils.load_image(content_image_path)
        starttime = time.time()
        content_tensor = utils.itot(content_image).to(device)
        generated_tensor = net(content_tensor)
        generated_image = utils.ttoi(generated_tensor.detach())
        if (PRESERVE_COLOR):
            generated_image = utils.transfer_color(content_image, generated_image)
        print("Transfer Time: {}".format(time.time() - starttime))
        # utils.show(generated_image)
        orig_filename = os.path.basename(content_image_path).split('.')[0]
        utils.saveimg(generated_image, f"images/results/{orig_filename}_new_style.jpg")

stylize(content_image_path)
